export interface ReduxStoreState {
  readonly searchedListings: {
    [key: string]: Listing;
  };
  readonly searchedLocations: {
    [key: string]: SearchedLocations;
  };
  readonly listingDetails: {
    [key: string]: ListingPageDetails;
  };
  readonly locationSearchLoadingStatus: {
    [key: string]: LocationLoadingStatus;
  };
  readonly pageStatus: PageStatus;
}

export interface PageStatus {
  page: string;
  currentLocation: string | null;
  currentListing: string | null;
}

export interface LocationLoadingStatus {
  loading: boolean;
  loaded: boolean;
}

export interface Listing {
  id: string;
  title: string;
  thumbImage: string;
  fullImage: string;
  address: Address;
  price: number;
  extraDetails?: Array<ExtraDetail>;
}

export interface ExtraDetail {
  title: string;
  text: string | number;
}
export interface Address {
  firstLine: string;
  secondLine?: string;
  city: string;
  postCode?: string;
  country: string;
}
export interface ListingPageDetails {
  id: string;
  heading: string;
  description: string;
  moreText: string;
}

export interface SearchedLocations {
  hasBeenSearched: boolean;
  listingIds: Array<string>;
}
