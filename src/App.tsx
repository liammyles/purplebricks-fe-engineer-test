import React, { Component } from "react";
import PageContainer from "./components/PageContainer";

const App = () => <PageContainer />;

export default App;
