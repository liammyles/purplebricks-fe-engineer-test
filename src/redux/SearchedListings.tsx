import { AnyAction } from "redux";
import { Listing } from "../typescript/interfaces";

// Actions
export const ADD_LISTINGS = "searchedListings/ADD_LISTINGS";

// Action Creators
export function addListings(listings: Array<Listing>) {
  let listingsObject: object | any = {};
  listings.forEach(item => {
    listingsObject[item.id] = item;
  });
  return { type: ADD_LISTINGS, payload: { listings: listingsObject } };
}

// Reducer
const defaultState = {};

export function searchedListings(
  state: { [key: string]: Listing } = defaultState,
  { type, payload }: AnyAction
) {
  switch (type) {
    case ADD_LISTINGS: {
      const { listings } = payload;
      return {
        ...state,
        ...listings
      };
    }
    default:
      return state;
  }
}
