import { isLoading, hasLoaded } from "./LocationLoadingStatus";
import { addListingIds, locationSearched } from "./SearchedLocations";
import { addListings } from "./SearchedListings";
import { addListingDetails } from "./ListingDetails";

// Action Creators for Fetch events

// Was having issue with using redux-trunk with typescript so left it out for now

/**
 * Triggers a request for the location details of the location passed.
 * This then fires off all of the dispatches that need the data from the
 * fetch request.
 *
 * @param {string} location
 */
export function getLocationListings(location) {
  return dispatch => {
    dispatch(isLoading(location));
    return fetch(`./data/${location}SearchListing.json`)
      .then(response => response.json())
      .then(json => {
        setTimeout(() => {
          dispatch(hasLoaded(location));
          dispatch(locationSearched(location));
          dispatch(addListings(json.listings));
          dispatch(addListingIds(location, json.listings));
        }, 500);
      })
      .catch(error => {
        throw Error(error);
      });
  };
}

/**
 * Triggers the request for the listing pages details and dispatches it
 *
 * @param {string} location
 */
export function getLocationListingDetails(location) {
  return dispatch => {
    return fetch(`./data/${location}ListingPages.json`)
      .then(response => response.json())
      .then(json => {
        setTimeout(() => {
          dispatch(addListingDetails(json.listingDetails));
        }, 500);
      })
      .catch(error => {
        throw Error(error);
      });
  };
}
