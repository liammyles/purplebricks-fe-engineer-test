import { AnyAction } from "redux";
import { ListingPageDetails } from "../typescript/interfaces";

// Actions
export const ADD_LISTING_DETAILS = "listingDetails/ADD_LISTING_DETAILS";

// Action Creators
export function addListingDetails(listingsDetails: Array<ListingPageDetails>) {
  const listingDetails: object | any = {};
  listingsDetails.forEach((listing: ListingPageDetails) => {
    listingDetails[listing.id] = listing;
  });
  return { type: ADD_LISTING_DETAILS, payload: { listingDetails } };
}

// Reducer
const defaultSate: { [key: string]: ListingPageDetails } = {};

export function listingDetails(
  state = defaultSate,
  { type, payload }: AnyAction
) {
  switch (type) {
    case ADD_LISTING_DETAILS: {
      const {
        listingDetails
      }: { listingDetails: { [key: string]: ListingPageDetails } } = payload;
      return {
        ...state,
        ...listingDetails
      };
    }
    default:
      return state;
  }
}
