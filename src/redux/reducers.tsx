import { searchedListings } from "./SearchedListings";
import { searchedLocations } from "./SearchedLocations";
import { listingDetails } from "./ListingDetails";
import { locationLoadingStatus } from "./LocationLoadingStatus";
import { pageStatus } from "./PageStatus";
import { combineReducers } from "redux";

const pbApp = combineReducers({
  searchedListings,
  searchedLocations,
  listingDetails,
  locationLoadingStatus,
  pageStatus
});

export default pbApp;
