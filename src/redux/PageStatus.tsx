import { AnyAction } from "redux";
import { PageStatus } from "../typescript/interfaces";

// Actions
export const UPDATE_ACTIVE_PAGE = "pageStatus/UPDATE_ACTIVE_PAGE";
export const UPDATE_LOCATION = "pageStatus/UPDATE_LOCATION";
export const UPDATE_LISTING = "pageStatus/UPDATE_LISTING";

//Action Creators

export function updateActivePage(page: string) {
  return { type: UPDATE_ACTIVE_PAGE, payload: { page } };
}
export function updateLocation(currentLocation: string) {
  return { type: UPDATE_LOCATION, payload: { currentLocation } };
}
export function updateListing(currentListing: string) {
  return { type: UPDATE_LISTING, payload: { currentListing } };
}

//Reducer
const defaultState = {
  page: "home",
  currentLocation: null,
  currentListing: null
};

export function pageStatus(
  state: PageStatus = defaultState,
  { type, payload }: AnyAction
) {
  switch (type) {
    case UPDATE_ACTIVE_PAGE: {
      const { page } = payload;
      return { ...state, page };
    }
    case UPDATE_LOCATION: {
      const { currentLocation } = payload;
      return { ...state, currentLocation };
    }
    case UPDATE_LISTING: {
      const { currentListing } = payload;
      return { ...state, currentListing };
    }

    default:
      return state;
  }
}
