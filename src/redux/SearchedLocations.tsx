import { SearchedLocations, Listing } from "../typescript/interfaces";
import { AnyAction } from "redux";

// Actions
export const LOCATION_SEARCHED = "searchedLocations/LOCATION_SEARCHED";
export const ADD_LISTING_IDS = "searchedLocations/ADD_LISTING_IDS";

// Action Creators

export function locationSearched(location: string) {
  return { type: LOCATION_SEARCHED, payload: { location } };
}

export function addListingIds(location: string, listings: Array<Listing>) {
  const listingIds = listings.map(listing => {
    return listing.id;
  });
  return { type: ADD_LISTING_IDS, payload: { location, listingIds } };
}

// Reducer

/**
 * This default state really should be dynamically loading in all of the locations,
 * but this is just for a tech test so it will do for now. But I wouldn't
 * do it this way in production.
 */
const defaultSate: { [key: string]: SearchedLocations } = {
  ireland: {
    hasBeenSearched: false,
    listingIds: []
  },
  england: {
    hasBeenSearched: false,
    listingIds: []
  },
  california: {
    hasBeenSearched: false,
    listingIds: []
  }
};

export function searchedLocations(
  state = defaultSate,
  { type, payload }: AnyAction
) {
  switch (type) {
    case LOCATION_SEARCHED: {
      const { location } = payload;
      return {
        ...state,
        [location]: {
          ...state[location],
          hasBeenSearched: true
        }
      };
    }
    case ADD_LISTING_IDS: {
      const {
        location,
        listingIds
      }: { location: string; listingIds: Array<string> } = payload;
      return {
        ...state,
        [location]: {
          ...state[location],
          listingIds
        }
      };
    }
    default:
      return state;
  }
}
