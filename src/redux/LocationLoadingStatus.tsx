import { AnyAction } from "redux";
import { LocationLoadingStatus } from "../typescript/interfaces";

// Actions
export const IS_LOADING = "locationLoadingStatus/IS_LOADING";
export const HAS_LOADED = "locationLoadingStatus/HAS_LOADED";

// Action Creators
export function isLoading(location: string) {
  return { type: IS_LOADING, payload: { location } };
}

export function hasLoaded(location: string) {
  return { type: HAS_LOADED, payload: { location } };
}

// Reducer
const defaultSate: { [key: string]: LocationLoadingStatus } = {};

export function locationLoadingStatus(
  state = defaultSate,
  { type, payload }: AnyAction
) {
  switch (type) {
    case IS_LOADING: {
      const { location } = payload;
      return {
        ...state,
        [location]: {
          ...state[location],
          loading: true
        }
      };
    }
    case HAS_LOADED: {
      const { location } = payload;
      return {
        ...state,
        [location]: {
          ...state[location],
          loaded: true,
          loading: false
        }
      };
    }
    default:
      return state;
  }
}
