import React from "react";
import SearchedPageContainer from "./SearchPageContainer";
import { PageStatus } from "../typescript/interfaces";
import ListingDetailsPageContainer from "./ListingDetailsPageContainer";
import styles from "../styles/page.module.scss";

interface IPagePresentationProps {
  pageStatus: PageStatus;
}

const PagePresentation = ({ pageStatus: { page } }: IPagePresentationProps) => (
  <>
    <h1 className={styles["page-header"]}>Purple Bricks Technical Test</h1>
    {page === "home" ? <SearchedPageContainer /> : null}
    {page === "listing" ? <ListingDetailsPageContainer /> : null}
  </>
);

export default PagePresentation;
