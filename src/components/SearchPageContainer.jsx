import { connect } from "react-redux";
import {
  getLocationListings,
  getLocationListingDetails
} from "../redux/FetchRequests";
import {
  updateActivePage,
  updateListing,
  updateLocation
} from "../redux/PageStatus";
import SearchPagePresentation from "./SearchPagePresentation";

const mapStateToProps = ({
  searchedListings,
  searchedLocations,
  listingPageDetails,
  locationLoadingStatus,
  pageStatus
}) => {
  return {
    searchedListings,
    searchedLocations,
    listingPageDetails,
    locationLoadingStatus,
    pageStatus
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateLocation: location => {
      dispatch(updateLocation(location));
    },
    getListings: location => {
      dispatch(getLocationListings(location));
      dispatch(getLocationListingDetails(location));
    },
    loadListingPage: listingId => {
      dispatch(updateActivePage("listing"));
      dispatch(updateListing(listingId));
    }
  };
};

const SearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPagePresentation);

export default SearchContainer;
