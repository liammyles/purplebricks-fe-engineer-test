import { connect } from "react-redux";
import ListingDetailsPagePresentation from "./ListingDetailsPagePresentation";
import { updateActivePage } from "../redux/PageStatus";

const mapStateToProps = ({
  listingDetails,
  searchedListings,
  pageStatus: { currentListing }
}) => {
  return { listingDetails, searchedListings, currentListing };
};

const mapDispatchToProps = dispatch => {
  return {
    returnHome: () => {
      dispatch(updateActivePage("home"));
    }
  };
};

const ListingDetailsPagePContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ListingDetailsPagePresentation);

export default ListingDetailsPagePContainer;
