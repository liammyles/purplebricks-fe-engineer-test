import React from "react";
import { ListingPageDetails, Listing } from "../typescript/interfaces";
import { ExtraDetails, AddressField } from "./ListingGenerics";
import listingStyles from "../styles/listing.module.scss";

interface IListingDetailsPageContainer {
  listingDetails: { [key: string]: ListingPageDetails };
  searchedListings: { [key: string]: Listing };
  currentListing: string;
  returnHome: () => void;
}

const ListingDetailsPageContainer = (props: IListingDetailsPageContainer) => {
  const {
    listingDetails,
    searchedListings,
    currentListing,
    returnHome
  } = props;
  const listingInfo = {
    ...listingDetails[currentListing],
    ...searchedListings[currentListing]
  };
  const {
    heading,
    description,
    fullImage,
    title,
    price,
    moreText,
    extraDetails,
    address
  } = listingInfo;

  const goBack = () => {
    returnHome();
  };

  return (
    <main
      className={`${listingStyles.listing} ${
        listingStyles["listing--details"]
      }`}
    >
      <h1 className={listingStyles["listing__heading"]}>{heading}</h1>
      <img src={fullImage} alt={title} />
      <div>
        <p>
          <strong>Price:</strong>
          {price}
        </p>
        {extraDetails ? <ExtraDetails extraDetails={extraDetails} /> : null}
        <AddressField address={address} />
      </div>
      <p>{description}</p>
      <p>{moreText}</p>
      <button
        className={listingStyles["listing__button"]}
        type="button"
        onClick={goBack}
      >
        Return Home
      </button>
    </main>
  );
};

export default ListingDetailsPageContainer;
