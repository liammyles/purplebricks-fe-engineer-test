import React, { EventHandler, SyntheticEvent } from "react";
import {
  Listing,
  SearchedLocations,
  ListingPageDetails,
  LocationLoadingStatus,
  PageStatus
} from "../typescript/interfaces";

import { AddressField, ExtraDetails } from "./ListingGenerics";
import listingStyles from "../styles/listing.module.scss";
import cardWrapperStyles from "../styles/cardWrapper.module.scss";
import pageStyles from "../styles/page.module.scss";

interface ISearchPageProps {
  updateLocation: (location: string) => void;
  getListings: (location: string) => void;
  loadListingPage: (listing: string) => void;
  searchedListings: { [key: string]: Listing };
  searchedLocations: { [key: string]: SearchedLocations };
  listingPageDetails?: { [key: string]: ListingPageDetails };
  locationLoadingStatus?: { [key: string]: LocationLoadingStatus };
  pageStatus: PageStatus;
}

const SearchPagePresentation = (props: ISearchPageProps) => {
  const updateLocationCLick: EventHandler<
    SyntheticEvent<HTMLButtonElement>
  > = event => {
    const { updateLocation, getListings, searchedLocations } = props;
    const { location } = event.currentTarget.dataset;
    if (location) {
      updateLocation(location);
      if (!searchedLocations[location].hasBeenSearched) {
        getListings(location);
      }
    }
  };
  return (
    <>
      <nav className={pageStyles["page-nav"]}>
        <button
          type="button"
          onClick={updateLocationCLick}
          data-location="ireland"
        >
          Search Locations In Ireland
        </button>
        <button
          type="button"
          onClick={updateLocationCLick}
          data-location="england"
        >
          Search Locations In England
        </button>
      </nav>
      <SearchCardsWrapper
        searchedListings={props.searchedListings}
        pageStatus={props.pageStatus}
        searchedLocations={props.searchedLocations}
        loadListingPage={props.loadListingPage}
      />
    </>
  );
};

interface ISearchCardsWrapperProps {
  searchedListings: { [key: string]: Listing };
  pageStatus: PageStatus;
  searchedLocations: { [key: string]: SearchedLocations };
  loadListingPage: (listing: string) => void;
}

const SearchCardsWrapper = ({
  searchedListings,
  pageStatus,
  searchedLocations,
  loadListingPage
}: ISearchCardsWrapperProps) => {
  let jsx = null;
  const { currentLocation } = pageStatus;
  if (currentLocation && searchedLocations[currentLocation]) {
    jsx = (
      <main className={cardWrapperStyles["card-wrapper"]}>
        {searchedLocations[currentLocation].listingIds.map(listingId => {
          const listing = searchedListings && searchedListings[listingId];
          return (
            <CardListing
              listing={listing}
              key={listingId}
              loadListingPage={loadListingPage}
            />
          );
        })}
      </main>
    );
  }

  return jsx;
};

interface ICardListingProps {
  listing: Listing;
  loadListingPage: (listing: string) => void;
}

const CardListing = ({ listing, loadListingPage }: ICardListingProps) => {
  const loadListingDetailsPage: EventHandler<
    SyntheticEvent<HTMLButtonElement>
  > = event => {
    const { listingId } = event.currentTarget.dataset;
    if (listingId) {
      loadListingPage(listingId);
    }
  };
  return (
    <div className={listingStyles.listing}>
      <div>
        <h2 className={listingStyles["listing__heading"]}>{listing.title}</h2>
        <img src={listing.thumbImage} alt={listing.title} />
        <p>This costs: {listing.price}</p>
        <AddressField address={listing.address} />
        {listing.extraDetails ? (
          <ExtraDetails extraDetails={listing.extraDetails} />
        ) : null}
      </div>
      <button
        type="button"
        data-listing-id={listing.id}
        onClick={loadListingDetailsPage}
        className={listingStyles["listing__button"]}
      >
        View Details
      </button>
    </div>
  );
};

export default SearchPagePresentation;
