import React from "react";
import { Address, ExtraDetail } from "../typescript/interfaces";

interface IAddressFieldProps {
  address: Address;
}

export const AddressField = ({ address }: IAddressFieldProps) => (
  <>
    <h3>Address</h3>
    <ul>
      <li>
        <strong>First Line:</strong> {address.firstLine}
      </li>
      {address.secondLine ? (
        <li>
          <strong>Second Line:</strong> {address.secondLine}
        </li>
      ) : null}
      {address.postCode ? (
        <li>
          <strong>Post Code:</strong> {address.postCode}
        </li>
      ) : null}
      <li>
        <strong>City:</strong> {address.city}
      </li>
      <li>
        <strong>Country:</strong> {address.country}
      </li>
    </ul>
  </>
);

interface IExtraDetailsProps {
  extraDetails: Array<ExtraDetail>;
}

export const ExtraDetails = ({ extraDetails }: IExtraDetailsProps) => (
  <>
    <h3>Extra Bits</h3>
    <ul>
      {extraDetails.map((detail, index) => (
        <li key={`${detail.title.replace(/\s+/, "")}-${index}`}>
          {detail.title}: {detail.text}
        </li>
      ))}
    </ul>
  </>
);
