import { connect } from "react-redux";
import PagePresentation from "./PagePresentation";
import { ReduxStoreState } from "../typescript/interfaces";

const mapStateToProps = ({ pageStatus }: ReduxStoreState) => {
  return { pageStatus };
};

const PageContainer = connect(mapStateToProps)(PagePresentation);

export default PageContainer;
