import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import pdApp from "./redux/reducers";
import "./index.scss";
import App from "./App";
import { composeWithDevTools } from "redux-devtools-extension";
import "./index.scss";

const store = createStore(
  pdApp,
  composeWithDevTools(applyMiddleware(thunkMiddleware))
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
