# Purple Bricks technical test

This is a tech test used as part of a job interview at Purple Bricks. Started on 11/11/2018.

For details on what was asked for the technical test you can refer to [this test document](test-direction.md)

## Project Details

This project was made using create react app. Which comes with a bunch of quality of life development features.

## Running the Project

To run this project, use the command `yarn start` or `npm run start`