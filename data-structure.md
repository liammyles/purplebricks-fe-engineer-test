# Data Structure of the Store

## Requirements

The Redux store structure of the state must help meet the following:

- Must store the users search results
  - This should enable faster initial loading display of data for a listing page.
  - This should enable faster load times of repeated searches.
- Must store users previously selected searches.
- Full details of the results page.

## Redux Store Structure

```json
{
  "searchedListings": {
    "9df66": {
      "id": "9df66",
      "title": "I Am House",
      "thumbImage": "picture.thumb.com",
      "fullImage": "picture.full.com",
      "price": 10,
      "details": [
        { "extraDetails": "about the listing" },
        { "inKey": "value" },
        { "pairs": 0 }
      ]
    }
  },
  "searchedLocations": {
    "ireland": {
      "hasBeenSearched": true,
      "listingsIds": ["9df66", "3df86", "hexValue"]
    },
    "california": {
      "hasBeenSearched": true,
      "listingsIds": ["4df96", "3df86"]
    },
    "uk": {
      "hasBeenSearched": true,
      "listingsIds": ["3df86", "3df86"]
    }
  },
  "listingDetails": {
    "9df66": {
      "id": "9df66",
      "heading": "The bestest House, evvar!",
      "description": "Do I really need to write an essay for a tech test? No? Okay cool, thanks :)",
      "moreText": "I could have used pirate ipsum I suppose"
    }
  },
  "locationSearchLoadingStatus": {
    "ireland": { "loading": true, "loaded": "false" },
    "uk": { "loading": false, "loaded": "true" },
    "california": { "loading": false, "loaded": "false" }
  }
}
```

## Reasoning

The Store prototype is broken up into 4 sections, but are all connected.

### searchedListings

### searchedLocations

### listingDetails

### locationSearchLoadingStatus
